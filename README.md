# map-gen

Showcase creating a 2d map generated using a modified version of drunkard walk with clojure. 

## Features
1. Seeded randomness.
2. Config can be found in `resources/config.edn`.
3. Displayed using quil.

## Requirements
1. Leinegen > 2
2. Java 8

# map-gen

用clojure展示修改后的醉酒漫步版本生成的2d地图。

## 特征
1. 随机种子。
2. 配置文件可以在 `resources/config.edn`中找到。
3. 使用quil显示。

## 要求
1. Leinegen > 2
2. Java 8

![./doc/images/example1.png](./doc/images/example1.png)
![./doc/images/example2.png](./doc/images/example2.png)
