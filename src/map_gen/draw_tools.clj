(ns map-gen.draw-tools
  (:require [clojure.tools.trace :as t]
            [clojure.string :refer [join]]
            [map-gen.config :refer :all]
            [quil.core :as q
             :include-macros true]))

(defn draw-grid-text [cells]
  (->>
    (for [row cells]
      (->>
        (for [ele row]
          (if (= :full ele) "-" "x"))
        (join "")))
    (join "\n")
    (println)))

(defn scaled [val]
  (* val (:scale config)))

(defn draw-grid-quil [cells]
  (q/defsketch my
               :size [(scaled (:width config))
                      (scaled (:height config))]
               :features [:keep-on-top :exit-on-close]
               :draw
               (fn []
                 (q/background (get-in config [:gui :color :bg]))
                 (q/no-stroke)
                 (doseq [[row-index row] (map-indexed vector cells)
                         [ele-index ele] (map-indexed vector row)]
                   (q/with-fill (get-in config [:gui :color ele])
                                (q/rect (scaled ele-index) (scaled row-index) (:scale config) (:scale config)))))))
