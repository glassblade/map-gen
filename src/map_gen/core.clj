(ns map-gen.core
  (:require [clojure.tools.trace :as t]
            [clojure.string :refer [join]]
            [random-seed.core :refer :all]
            [map-gen.config :refer :all]
            [map-gen.draw-tools :refer :all]
            [map-gen.area :refer :all]
            [map-gen.util :refer :all]
            [quil.core :as q
             :include-macros true])
  (:refer-clojure :exclude [rand rand-int rand-nth])
  (:gen-class))

(defn full-grid
  [width height]
  (vec
    (repeat height
            (vec (repeat width :full)))))

(defn drunkards-walk
  [grid num-empty-cells]
  (let [height (count grid)
        width (count (first grid))]
    ; Guard against impossible demands.
    (when (<= num-empty-cells (* width height))

      (loop [grid grid
             ; Step 1: pick a random cell.
             x (rand-int width)
             y (rand-int height)
             empty-cells 0]

        ; Step 2: if we're done, return the grid.
        (if (= empty-cells num-empty-cells)
          grid

          (let [cell-was-full? (= (get-in grid [y x]) :full)
                ; Step 3: walk one step in a random direction.
                weights {:north (density grid (dec y) x)
                         :south (density grid (inc y) x)
                         :east  (density grid y (inc x))
                         :west  (density grid y (dec x))}
                direction (rand-nth
                            (flatten
                              [(repeat (weights :north) :north)
                               (repeat (weights :south) :south)
                               (repeat (weights :east) :east)
                               (repeat (weights :west) :west)]))]

            ; Step 4: back to step 2.
            (recur (assoc-in grid [y x] :empty)
                   (case direction
                     :east (bound-between (inc x) 0 (dec width))
                     :west (bound-between (dec x) 0 (dec width))
                     x)
                   (case direction
                     :north (bound-between (dec y) 0 (dec height))
                     :south (bound-between (inc y) 0 (dec height))
                     y)
                   (if cell-was-full?
                     (inc empty-cells)
                     empty-cells))))))))

(defn smooth [cells]
  (vec (for [[row-index row] (map-indexed vector cells)]
         (vec (for [[ele-index ele] (map-indexed vector row)]
                (if (>= 1 (density cells row-index ele-index))
                  :empty
                  ele))))))

(defn -main
  [& args]
  (set-random-seed-from-config!)
  (-> (full-grid (:width config) (:height config))
      (drunkards-walk (:steps config))
      (smooth)
      (locate-special-areas)
      (draw-grid-quil)))






