(ns map-gen.area
  (:require [map-gen.util :refer :all]
            [map-gen.config :refer :all]))

(defn neighbors
  ([size yx]
   (neighbors [[0 0] [1 1] [-1 -1] [-1 1] [1 -1] [-1 0] [1 0] [0 -1] [0 1]] size yx))
  ([deltas size yx]
   (filter
     (fn [new-yx]
       (every? #(< -1 % size) new-yx))
     (map #(vec (map + yx %))
          deltas))))

(defn density [matrix y x]
  (let [neighbors (map #(get-in matrix %) (neighbors (count matrix) [y x]))
        full-neighbors (:full (frequencies neighbors))]
    (if (nil? full-neighbors)
      -1
      (max 1 (* full-neighbors full-neighbors)))))

(defn mark-area [cells area-symbol valid-points]
  "mark 2x2 area"
  (->> valid-points
       ((fn [[item]] item))
       (apply mapv vector)
       (map sort)
       ((fn [[[x1 x2] [y1 y2]]] (for [i (range x1 (inc x2)) j (range y1 (inc y2))] [i j])))
       (apply-assoc-in cells area-symbol)))

(defn mark-areas
  ([cells area-symbols valid-points]
   (mark-areas cells area-symbols valid-points (count area-symbols)))
  ([cells area-symbols valid-points limit]
   (let [valid-points (take-randnth limit (vec valid-points))]
     (if (and (some? (first valid-points)) (some? (first area-symbols)))
       (mark-areas (mark-area cells (first area-symbols) (first valid-points)) (rest area-symbols) (rest valid-points))
       cells))))


(defn locate-special-areas [cells]
  (->> cells
       (map-indexed vector)
       (map (fn [[row-index row]]
              (->> row
                   (map-indexed vector)
                   (map
                     (fn [[ele-index ele]]
                       (let [all-neighbors (neighbors
                                             [[1 1] [-1 -1] [-1 1] [1 -1] [-1 0] [1 0] [0 -1] [0 1]]
                                             (count cells)
                                             [row-index ele-index])
                             neighbour-values (map #(get-in cells %) all-neighbors)
                             full-neighbour-index (.indexOf neighbour-values :full)]
                         (if (and
                               (= 7 (:empty (frequencies neighbour-values)))
                               (> 4 full-neighbour-index)
                               (= :full ele))
                           [[row-index ele-index] (nth all-neighbors full-neighbour-index)]
                           nil))))
                   (filter some?))))
       (filter not-empty)
       (mark-areas cells (:area-tags config))))