(ns map-gen.util
  (:require [clojure.tools.trace :as t]
            [random-seed.core :refer :all])
  (:refer-clojure :exclude [rand rand-int rand-nth]))

(defn apply-assoc-in [vvec result indexes]
  (if (some? (first indexes))
    (apply-assoc-in (assoc-in vvec (first indexes) result) result (rest indexes))
    vvec))

(defn remove-at
  "remove elem in coll"
  [pos coll]
  (vec (concat (subvec coll 0 pos) (subvec coll (inc pos)))))

(defn take-randnth
  ([limit coll] (take-randnth limit coll []))
  ([limit coll result]
   (if (< 0 (count coll))
     (let [n (rand-int (count coll))]
       (take-randnth limit (remove-at n coll) (conj result (nth coll n))))
     (vec result))))

(defn bound-between
  [number lower upper]
  (cond
    (< number lower) lower
    (> number upper) upper
    :else number))


