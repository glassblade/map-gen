(ns map-gen.config
  (:require [clojure.tools.trace :as t]
            [clojure.java.io :as io]
            [clojure.edn :as edn]
            [random-seed.core :refer :all])

  (:refer-clojure :exclude [rand rand-int rand-nth]))

(def config (edn/read-string (slurp (io/resource "config.edn"))))
(defn set-random-seed-from-config! [] (set-random-seed! (:random-seed config)))